var id = 0;
$(document).ready(function() {
    $.ajax({
        method: 'GET',
        url: environment_variables.api.topico + "1"
    }).done(function(response) {

        var data = response.data

        data.forEach(function(element) {

            $("#topicosSelector").append('<option value=' + element.IdTopico + '>' + element.Tipo + '</option>')


        });
    })


    $(document).ready(function() {

        $('#topicosSelector').on('change', function() {

            var valoreSeleccionado = $(this).children(":selected").val();
            llenarDataTopicos(valoreSeleccionado);
            guardarId(valoreSeleccionado);

        });
    });

    function guardarId(valoreSeleccionado) {

        id = valoreSeleccionado;

    }

    $(document).ready(function() {

        $("#confgAnswer").click(enviarIdTopico)

    });

    function enviarIdTopico() {

        return id;

    }

    var llenarDataTopicos = function(valoreSeleccionado) {

        var idTopico = valoreSeleccionado;

        $.ajax({
            method: 'GET',
            url: environment_variables.api.dataTopicos + idTopico,

            beforeSend: function() {

                $('#contestaciones').empty()
                $('#palabraUno').empty()
                $('#palabraDos').empty()

            }

        }).done(function(respuesta) {

            var contestaciones = respuesta.contestaciones
            var Uno = respuesta.claveUno
            var Dos = respuesta.claveDos

            //console.log(contestaciones);

            var ite = 0
            contestaciones.forEach(function(element) {


                $('#contestaciones').append('<p class="card-text">' + (ite + 1) + ' ' + element['frase'] + '</p>');

                ite++;

            });

            ite = 0
            Uno.forEach(function(wordOne) {

                $('#palabraUno').append('<p class="card-text">' + (ite + 1) + ' ' + wordOne['frase'] + '</p>');

                ite++
            });

            ite = 0
            Dos.forEach(function(wordTwo) {

                $('#palabraDos').append('<p class="card-text">' + (ite + 1) + ' ' + wordTwo['frase'] + '</p>');

                ite++
            });



        });

    }


    /*$("#formulario").on('submit', function() {
        var payload = {};
        payload['nombre'] = $("#botname").val();
        payload['descripcion'] = $("#botdescription").val();
        payload = JSON.stringify(payload);

        $.ajax({
            method: 'POST',
            url: environment_variables.api.saludo + "/1",
            data: payload
        }).done(function(response) {
            console.log(response);
        })
    });*/
});