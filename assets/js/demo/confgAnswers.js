$(document).ready(function() {

    llenarDocumento();

    $("#NewText").on('click', function() {
        $("#tokenNuevo").val("");
        $("#modalNuevo").modal('show');
    });
    $("#btnGuardarToken").on('click', function() {
        guardarToken();
    });

    $(".close-modal").on('click', cerrarModal);
});

function myFunction(id) {

    var dato = id;
    var payload = {};
    payload["idContestacion"] = dato;
    payload["estado"] = "I";
    $.ajax({

        method: 'POST',
        url: environment_variables.api.borrarRespuestaTopico,
        data: payload,
        beforeSend: function() {

            $('#listFrases').empty()

        }

    }).done(function(response) {

        llenarDocumento();

    })

}


function llenarDocumento() {

    $.ajax({
        method: 'GET',
        url: environment_variables.api.dataTopicos + "1"
    }).done(function(response) {

        var data = response.contestaciones

        data.forEach(function(element) {
            var input = document.createElement('textarea');
            $(input)
                .attr('id', element.idContestacion)
                .addClass('form-control form-control-sm')
                .val(element.frase);

            var btnEliminar = document.createElement('button');
            $(btnEliminar)
                .attr('type', 'button')
                .addClass('btn btn-danger btn-sm')
                .append('Eliminar')
                .on('click', function() {
                    myFunction(element.idContestacion);
                });

            var divAppend = document.createElement('div');
            $(divAppend)
                .addClass('input-group-append')
                .append(btnEliminar)

            var divInput = document.createElement('div');
            $(divInput)
                .addClass('input-group mb-3')
                .append(input, divAppend)
                .appendTo('.listTopic');
        });

    })
}

function guardarToken() {
    var palabra = $("#tokenNuevo").val();
    var idTopico = "1";
    var payload = {
        "frase": palabra,
        "idTopico": idTopico
    };
    $.ajax({
        method: "POST",
        url: environment_variables.api.agregarContestaction,
        data: payload
    }).done(function(response) {
        if (response.Success) {
            alert("Token Agregado exitosamente");
            $(".listTopic").empty();
            llenarDocumento();
            $("#modalNuevo").modal('hide');
        }
    });
}

function cerrarModal() {
    $("#modalNuevo").modal('hide');
}