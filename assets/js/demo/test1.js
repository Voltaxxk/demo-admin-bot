$(document).ready(function() {
    $.ajax({
        method: 'GET',
        url: environment_variables.api.datos
    }).done(function(response) {
        $("#botname").append(response.data.Nombre);
        $("#descripcion").append(response.data.Descripcion);
    })

    $("#formulario").on('submit', function() {
        var payload = {};
        payload['nombre'] = $("#botname").val();
        payload['descripcion'] = $("#botdescription").val();
        payload = JSON.stringify(payload);

        $.ajax({
            method: 'POST',
            url: environment_variables.api.saludo + "/1",
            data: payload
        }).done(function(response) {
            console.log(response);
        })
    });
});