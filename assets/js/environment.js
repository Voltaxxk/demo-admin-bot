var environment = 'development';
var baseUrlMain = window.location.protocol + "//" + window.location.hostname;
var apiUrlMain = "";
if (environment == "development") {
    apiUrlMain = "http://localhost/calbot/api";
}
var partsDomain = window.location.hostname.split('.');
var subdomainSite = partsDomain.shift();

var environment_variables = {
    "api": {
        "datos": apiUrlMain + "/bot",
        "saludo": apiUrlMain + "/bot/saludar/",
        "despedir": apiUrlMain + "/bot/despedir/",
        "topico": apiUrlMain + "/topicos/",
        "dataTopicos": apiUrlMain + "/topicos/data/",
        "agregarTokenUno": apiUrlMain + "/keyWordOne/newKeyWordOne",
        "agregarTokenDos": apiUrlMain + "/keyWordTwo/newKeyWordTwo",
        "agregarContestaction": apiUrlMain + "/contestaciones/newAnswers",
        "agregarSaludo": apiUrlMain + "/saludos/newSalute",
        "agregarDespedida": apiUrlMain + "/despedidas/newFarewell",
        "borrarRespuestaTopico": apiUrlMain + "/contestaciones/editAnswers",
        "borrarPalabraClaveUno": apiUrlMain + "/keyWordOne/editKeyWordOne",
        "borrarPalabraClaveDos": apiUrlMain + "/keyWordTwo/editKeyWordTwo",
        "borrarSaludos": apiUrlMain + "/saludos/editSalute",
        "borrarDespedidas": apiUrlMain + "/despedidas/editFarewell"
    }
}